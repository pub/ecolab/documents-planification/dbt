with scots as (
    select * from {{ source('brut', 'scots_wfs') }}
)

, scots_id as (
    select * from {{ source('brut', 'scots_id_wfs') }}
)

, infos_scots as (
    select
        s.partition as code_doc
        , null as structure_porteuse
        , case
            when id.title is not null then id.title
            else s.nom
        end
        as nom_doc
        , {{ replace_etat('s.etat', "'SCoT'", 'null') }} as etat_document
        , s.siteweb as site_web
        , CONCAT('https://www.geoportail-urbanisme.gouv.fr/document/by-id/', id.gpu_id) as lien_centralise
        , null as liens_docs
        , s.typeproc as type_evt
        , null as date_evt
        , s.datappro as date_creation
        , s.datefin as date_expiration
        , 'SCoT' as type_doc
        , null as contact
    from scots s
    left join scots_id id on s.partition = id.partition
    where s.etat != '04' and s.etat != '06' and s.etat != '09'
)

, infos_scots_v2 as (
    select
        id.partition as code_doc
        , id.title as structure_porteuse
        , id.title as nom_doc
        , s.siteweb as site_web
        , case
            when
                id.gpu_id is null then null
                else CONCAT('https://www.geoportail-urbanisme.gouv.fr/document/by-id/', id.gpu_id)
        end as lien_centralise
        , case
            when s.urlpadd is not null or s.urldoo is not null or s.urlrapport is not null or s.urlpe is not null
            then CONCAT(
                case
                    when s.urlpadd is not null then CONCAT('padd : ',s.urlpadd, ';')
                    else ''
                end,
                case
                    when s.urldoo is not null then CONCAT('doo : ',s.urldoo, ';')
                    else ''
                end,
                case
                    when s.urlrapport is not null then CONCAT('rapport : ',s.urlrapport,';')
                    else ''
                end,
                case
                    when s.urlpe is not null then CONCAT('pe : ',s.urlpe,';')
                    else ''
                end, ''
            )
            else null
        end as liens_docs
        , case 
            when s.partition is not null then s.etat
            else id.approved::text
        end as etat_document
        , null as sous_etat
        , {{dernier_evt('s','typeproc')}} as type_evt
        , null as date_evt
        , s.datappro as date_creation
        , s.datefin as date_expiration
        , 'SCoT' as type_doc
        , null as contact
    from scots_id id
    left join scots s on s.partition = id.partition
    where (s.etat != '04' and s.etat != '06' and s.etat != '09' and s.etat != '05') or s.partition is null
    union
        select
        s.partition as code_doc
        , s.nom as structure_porteuse
        , s.nom as nom_doc
        , s.siteweb as site_web
        , null as lien_centralise
        , null as liens_docs
        , s.etat as etat_document
        , null as sous_etat
        , {{dernier_evt('s','typeproc')}} as type_evt
        , null as date_evt
        , s.datappro as date_creation
        , s.datefin as date_expiration
        , 'SCoT' as type_doc
        , null as contact
    from scots s
    left join scots_id id on s.partition = id.partition
    where s.etat != '04' and s.etat != '06' and s.etat != '09' and s.etat != '05' and id.partition is null
)

select * from infos_scots_v2
