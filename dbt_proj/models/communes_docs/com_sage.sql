with brut_communes_sages as (
    select 
        code_insee as codegeo
        , concat(code_insee, ';-;',code) as id
        , code as code_doc
        , 'SAGE' as type_doc
    from {{source('brut','com_sages')}}
)

, i_sage as (
    select code_doc
    from {{ ref('i_sage' )}}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

{{ changement_codegeo('brut_communes_sages', 'codegeo', 'id', 'passage', 2023) }}

select distinct on(code_doc, codegeo)
    i.code_doc as code_doc
    , m.new_codegeo as codegeo
    , 'SAGE' as type_doc
from macro_brut_communes_sages m
join i_sage i
on substr(m.id, position(';-;' in m.id) + 3) = i.code_doc
