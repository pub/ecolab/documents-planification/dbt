# Coding Conventions

## dbt Coding Conventions

We will follow almost all coding conventions provided by dbt
[here](https://github.com/fishtown-analytics/corp/blob/master/dbt_coding_conventions.md).

**Important ones**

- We rename and cast only once
- DO NOT optimize for a smaller number of lines of code: newlines are cheap, brain time is expensive.
- All column names should be in snake case
<!-- - All primary keys have the table prefix -->
- All IDs should be cast as string
- We apply TRIM to all string of text such as description or address street
- We cast all dates as `DATE` if they are in the format `2020-01-01T00:00:00`. Their suffix is `_date`. (ex: `created_date`)
- We cast all datetime/timestamps as `TIMESTAMP` in `UTC` if they are in the format `2020-01-01T01:04:51`.
 We apply the transformation `TIMESTAMP(<column_dt>, 'Europe/Paris')` if necessary. Their suffix is `_at`. (ex: `created_at`)
<!-- - DO NOT use aliases for tables (it messes up with the drop_deprecated_models macro) -->

**Languages**

- We keep source naming and translate them only in the core dim layer.

## Model Naming and Repo Organisation

```
├── dbt_project.yml
├── profiles.yml
└── docs
|   └── colums_definitions.md
└── models
|   ├── marts
|   |   └── mart_schema.yml
|   |   └── ..........sql
|   └── ref
|       ├── ref_schema.yml
|       ├── ref_source.yml
|       └── ref_..........sql
|   └── staging
|       ├── stg_schema.yml
|       ├── stg_sources.yml
|       └── stg_..........sql
└── seed
    ├── 
├── .gitlab-ci.yml
├── .main.env
├── .pre-commit-config.yml
├── .sqlfluff
├── README.md
├── requirements.txt
```

### The staging area

The staging area aims at defining the source and cleansing the raw data to be used for modeling purposes.
The table are materialised in the dataset `stg_dbt`.

- **Fivetran**

For Fivetran Tables, we remove deleted rows if they exist, using the following templates:

```yaml
WITH source AS (

  SELECT * FROM {{ source('my_source', 'my_table') }}

  WHERE <condition>

),

renamed AS (

  SELECT
    --my fields here with renaming/casting as appropriate

  FROM source

)

SELECT * FROM renamed
```

- **Custom Ingestion**

This stage is the place to rename and cast our columns.

### The marts area

The marts area corresponds to the area where our data can be consumed by other services such as Looker or Cumul IO.
In this area, tables are built according to the service that will consume them.


#### Core tables

Correspond to the intelligence layer of the DWH.
It contains the core tables that structure and describe our activities, and may be used across different marts.

You will find tables from specific source in folder `marts/core/<source>/`.
Tables that are cross-source are located at the root of folder `marts/core/`.

In some cases where the entity only exists in one source (such as Leads on Salesforce), it will be directly saved in folder `marts/core`
without it source-specific table in folder `marts/core/<source>/`.

**fact** vs **dim**

Fact tables are referencing data coming from multiple entities (dimensions): order, products etc...
Within Looker, most measures will be added to these tables, that's why they are called facts.
