with pdm as (
    select * from {{ source('brut', 'pdm') }}
)

select
    nom_de_l_aom as structure_porteuse
    , concat('pdm_', identifiant_du_plan, '_', annee_d_approbation__finalisation, '_', type_de_plan) as code_doc
    , concat(nom_court_de_l_aom, ' ', annee_d_approbation__finalisation) as nom_doc
    , case
        when annee_d_approbation__finalisation is null
            then 'En élaboration'
        else 'Approuvé'
    end as etat_document
    , type_de_plan as sous_etat
    , null as site_web
    , replace(
        concat(
            'https://plans-mobilite.cerema.fr/'
            , replace(
                replace(
                    replace(
                        replace(
                            lower(
                                replace_special_characters(nom_de_l_aom)
                            )
                            , '''', '-'
                        ), ' ', '-'
                    ), '(', '-'
                ), ')', '-'
            ), case
                when annee_d_approbation__finalisation is not null
                    then '-'
            end
            , annee_d_approbation__finalisation
        ), '--', '-'
    ) as lien_centralise
    , null as liens_docs
    , case
        when annee_d_approbation__finalisation is not null
            then 'Approbation du plan'
    end as type_evt
    , annee_d_approbation__finalisation as date_evt
    , null as date_creation
    , null as date_expiration
    , 'PDM' as type_doc
    , null as contact
from pdm
