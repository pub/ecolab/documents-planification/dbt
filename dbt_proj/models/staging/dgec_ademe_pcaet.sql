with siren_pcaet as (
    select
        *
        , replace(
            replace(
                replace(
                    replace(
                        upper(
                            replace_special_characters(nom_du_groupement)),' ','')
                    ,'-','')
                ,'’','')
            ,'''','') as collectivite_norm
        , replace(
            replace(
                replace(
                    replace(
                        upper(
                            replace_special_characters(structure_porteuse)),' ','')
                    ,'-','')
                ,'’','')
            ,'''','') as porteur_norm
    from {{ ref('siren_pcaet') }}
)

, ref_collectivites_annees as (
    select * from {{ ref('ref_collectivites_annees') }}
)

, donnees_ademe_temp as (
    select * from {{ source('brut', 'demarches_pcaet_v1') }}
    union
    select * from {{ source('brut', 'demarches_pcaet_v2') }}
)

, ademe_sans_doublons as (
    select max(id::int)::text as id from donnees_ademe_temp
    group by siren_collectivites_coporteuses
    having count(*)>1
    union
    select max(id::int)::text as id from donnees_ademe_temp
    group by siren_collectivites_coporteuses
    having count(*)=1
    union
    select id from donnees_ademe_temp
    where siren_collectivites_coporteuses is null
)

, donnees_ademe as(
    select t1.*
    from donnees_ademe_temp t1
    inner join ademe_sans_doublons
    using(id)
)

, fichiers_demarches as (
    select * from {{ source('brut', 'pcaet_demarches_fichiers') }}
)

, jointure_structure_porteuse_siren as (
    select
        siren.siren
        , siren.structure_porteuse
        , siren.siren_structure_porteuse
        , ademe.siren_collectivites_coporteuses as siren_ademe
        , ademe.id
    from siren_pcaet siren
    join donnees_ademe ademe
    on siren.siren_structure_porteuse = ademe.siren_collectivites_coporteuses
    where siren.siren_structure_porteuse is not null
)

, jointure_structure_non_porteuse_siren as (
    select
        siren.siren
        , siren.structure_porteuse
        , siren.siren_structure_porteuse
        , ademe.siren_collectivites_coporteuses as siren_ademe
        , ademe.id
    from siren_pcaet siren
    join donnees_ademe ademe
    on siren.siren = ademe.siren_collectivites_coporteuses
    where siren.siren != siren.siren_structure_porteuse or siren.siren_structure_porteuse is null
)

, tous_les_id as (
    select * from jointure_structure_porteuse_siren
    union
    select * from jointure_structure_non_porteuse_siren
)

, pcaet_ademe_restant as (
    select
        id.id as id_dgec
        , ademe.*
    from donnees_ademe ademe
    left join tous_les_id id
        on ademe.id = id.id
    where id.id is null
)

, id_restants as (
    select
        replace(
            replace(
                replace(
                    replace(
                        upper(
                            replace_special_characters(ademe.collectivites_porteuses)),' ','')
                    ,'-','')
                ,'’','')
            ,'''','') as nom_norm
        , ademe.*
    from donnees_ademe ademe
    join pcaet_ademe_restant
    using(id)
)

select * from tous_les_id
