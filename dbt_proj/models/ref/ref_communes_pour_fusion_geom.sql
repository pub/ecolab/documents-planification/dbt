with contours as (
    select * from {{source('brut','communes_ign')}}
)

, contours_modif as (
    select 
        insee_com
        , case 
            when ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.002) is not null
                then ST_MakeValid(ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.002))
            when ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.001) is not null
                then ST_MakeValid(ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.001))
            else ST_MakeValid(ST_SimplifyPreserveTopology(geometry,4326))
        end as geom
    from contours
)

select * from contours_modif