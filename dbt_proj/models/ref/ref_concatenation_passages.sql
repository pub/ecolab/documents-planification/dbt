    with macro_concatenation_passages as (
        select 
            codgeo_2023 as codegeo
            , 2023 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2022 as codegeo
            , 2022 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2021 as codegeo
            , 2021 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2020 as codegeo
            , 2020 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2019 as codegeo
            , 2019 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2018 as codegeo
            , 2018 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2017 as codegeo
            , 2017 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2016 as codegeo
            , 2016 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2015 as codegeo
            , 2015 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2014 as codegeo
            , 2014 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2013 as codegeo
            , 2013 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2012 as codegeo
            , 2012 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2011 as codegeo
            , 2011 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}
        union
        select 
            codgeo_2010 as codegeo
            , 2010 as annee
            , codgeo_2023 as codegeo_cible
        from {{ source('brut','passage_2023') }}

    )

select * from macro_concatenation_passages
