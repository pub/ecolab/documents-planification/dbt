with com_docs as (
    select * from {{ ref('com_docs') }}
)

, ref_com as (
    select * from {{ref('ref_communes')}}
)

, pop_superf as (
    select 
    com_docs.code_doc
    , com_docs.type_doc
    , sum(ref_com.superficie) as superficie
    , sum(ref_com.pop) as pop
    from com_docs
    left join ref_com
    on ref_com.codegeo = com_docs.codegeo
    group by code_doc, type_doc
)

select * from pop_superf
