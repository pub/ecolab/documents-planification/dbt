with pcaet as (
    select * from {{ ref('doublons_pcaet') }}
)

, ademe_v1 as (
    select * from {{ source('brut', 'demarches_pcaet_v1') }}
)

, ademe_v2 as(
    select * from {{ source('brut', 'demarches_pcaet_v2') }}
)

, ademe as (
    select * from ademe_v1
    union
    select * from ademe_v2
)

, url_fichiers as (
    select * from {{ source('brut', 'pcaet_demarches_fichiers') }}
)

, lien_centralise_temp_v1 as (
    select
        id
        , concat('https://www.territoires-climat.ademe.fr/'
            , replace(
                replace(lower(replace_special_characters(nom)), '''', '')
            , ' ', '-'
            ), '/demarche'
        ) as url_fiche
    from ademe_v1
)

, lien_centralise_temp_v2 as (
    select
        id
        , concat ('https://www.territoires-climat.ademe.fr/', id, '-'
                , replace(
                    replace(lower(replace_special_characters(nom)), '''', '')
                , ' ', '-'
                ), '/demarche'
        ) as url_fiche
    from ademe_v2
)

, liens_centralise as (
    select * from lien_centralise_temp_v1
    union
    select * from lien_centralise_temp_v2
)

, liens_fichiers as (
    select
        demarche_id as id
        , concat(display_name, ' : ', 'https://www.territoires-climat.ademe.fr/', lien_fichier) as lien_doc
    from url_fichiers
)

, infos_pcaet as (
    select
        pcaet.structure_porteuse
        , concat('PCAET_'
            , replace(
                replace(
                    replace(
                        replace(
                            lower(
                                replace_special_characters(pcaet.structure_porteuse)
                                )
                        ,' ','')
                    ,'-','')
                ,'’','')
            ,'''','')
        ) as code_doc
        , concat('PCAET de ', pcaet.structure_porteuse) as nom_doc
        , case
            when max(pcaet.date_approbation) is not null or max(pcaet.scot_valant_pcaet) is not null
                then 'Adopté'
            when min(pcaet.lancement_demarche_0_1_) = '1'
                then 'En élaboration'
            else 'Non démarré'
        end as etat_document
        , case
            when max(pcaet.oblige_volontaire) = 'non-obligé' or max(pcaet.oblige_volontaire) = 'Volontaire'
                then 'Volontaire'
            else 'Obligé'
        end as sous_etat
        , null as site_web
        , null as lien_centralise
        , string_agg(distinct(fichier.lien_doc), ';') as liens_docs
        , case
            when max(pcaet.date_approbation) is not null
                then 'Approbation du PCAET'
            when max(pcaet.date_avis_etat) is not null
                then 'Avis état'
            when max(pcaet.date_avis_ae) is not null
                then 'Avis ae'
            when max(pcaet.date__lancement) is not null
                then 'lancement démarche'
        end as type_evt
        , coalesce(max(pcaet.date_approbation), max(pcaet.date_avis_etat), max(pcaet.date_avis_ae), max(pcaet.date__lancement)) as date_evt
        , max(pcaet.date_approbation) as date_creation
        , null as date_expiration
        , 'PCAET' as type_doc
        , null as contact
    from pcaet
    left join liens_fichiers fichier
        using(id)
    left join liens_centralise centr
        on centr.id = pcaet.id
    group by pcaet.structure_porteuse
)

select * from infos_pcaet