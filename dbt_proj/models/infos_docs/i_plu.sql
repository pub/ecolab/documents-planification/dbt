with du as (
    select * from {{ source('brut', 'docs_urba_wfs') }}
    where typedoc = 'PLU'
)

, du_id as (
    select * from {{ source('brut', 'docs_urba_id_wfs') }}
    where du_type = 'PLU'
)

, du_infos as (
    select distinct on(du_id.partition)
        du_id.grid_title as structure_porteuse
        , concat('PLU_', du_id.partition) as code_doc
        , case
            when right(du_id.partition, 1) = 'A'
                then concat(du_id.du_type, ' ', du_id.grid_title, ' partie A')
            when right(du_id.partition, 1) = 'B'
                then concat(du_id.du_type, ' ', du_id.grid_title, ' partie B')
            else CONCAT(du_id.du_type, ' ', du_id.grid_title)
        end as nom_doc
        , {{replace_etat('du1.etat', "'00000'","'00000'")}} as etat_document
        , null as sous_etat
        , du1.siteweb as site_web
        , CONCAT('https://www.geoportail-urbanisme.gouv.fr/document/by-id/', du_id.id) as lien_centralise
        , null as liens_docs
        , {{dernier_evt('du1', 'nomproc')}} as type_evt
        , du1.datappro as date_evt
        , null as date_creation
        , du1.datefin as date_expiration
        , 'PLU' as type_doc
        , null as contact
        , du_id.grid_name as codegeo
    from du_id
    left join (
        select distinct on(du.datappro,du.partition) 
            du.* 
        from du
        inner join (
            select distinct on(du.partition) 
                du.partition
                , du.datappro
            from (
                select 
                    MAX(TO_DATE(datappro,'YYYYMMDD')) as datappro
                    , partition
                from du 
                group by partition
            ) t1
            inner join du 
            on (
                TO_DATE(du.datappro,'YYYYMMDD') = t1.datappro 
                and du.partition = t1.partition
            )
        ) t2 
        on (
            t2.partition = du.partition 
            and t2.datappro = du.datappro
        )
    ) du1
    on du1.partition = du_id.partition
)

select * from du_infos
