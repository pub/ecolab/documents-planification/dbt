with nom_sdages as (
    select * from {{source('brut','nom_sdages')}}
)

, liens_sdages as (
    select * from {{source('brut','lien_sdage')}}
)

, infos_sdages as (
    select
        ns.nomcircadminbassin as structure_porteuse
        , CONCAT('SDAGE_',ns.numcircadminbassin) as code_doc
        , CONCAT('SDAGE ',ns.nomcircadminbassin) as nom_doc
        , 'Validé' as etat_document
        , null as sous_etat
        , ls.lien as site_web
        , null as lien_centralise
        , null as liens_docs
        , null as type_evt
        , null as date_evt
        , '2022' as date_creation
        , '2027' as date_expiration
        , 'SDAGE' as type_doc
        , 'null' as contact
        from nom_sdages ns
        left join liens_sdages ls on ns.nomcircadminbassin = ls.nom_sdage
)

select * from infos_sdages
