with brut_sraddet as (
    select * from {{ source('brut', 'sraddet') }}
)

, communes as (
    select
        codegeo
        , reg
    from {{ ref('ref_communes') }}
)

select
    concat('SRADDET_', b.insee) as code_doc
    , c.codegeo as codegeo
    , 'SRADDET' as type_doc
from brut_sraddet b
inner join communes c
    on replace(b.insee, ' ', '') = c.reg
