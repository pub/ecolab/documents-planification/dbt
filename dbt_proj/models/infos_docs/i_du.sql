with du as (
    select * from {{ source('brut', 'docs_urba_wfs') }}
    where typedoc = 'CC' or typedoc = 'PLU' or typedoc = 'PLUi' or typedoc = 'PLUI'
)

, du_id as (
    select * from {{ source('brut', 'docs_urba_id_wfs') }}
    where du_type = 'CC' or du_type = 'PLU' or du_type = 'PLUi' or du_type = 'PLUI'
)

, du_infos as (
    select distinct on(du_id.partition)
        du_id.grid_title as structure_porteuse
        , du_id.partition as code_doc
        , case
            when du1.typedoc is not null 
                then CONCAT(du1.typedoc, ' ', du_id.grid_title)
            else CONCAT(du_id.du_type, ' ', du_id.grid_title)
        end as nom_doc
        , du1.etat as etat_document
        , case
            when du1.typedoc is not null 
                then du1.typedoc
            else du_id.du_type
        end as sous_etat
        , du1.siteweb as site_web
        , CONCAT('https://www.geoportail-urbanisme.gouv.fr/document/by-id/', du_id.id) as lien_centralise
        , null as liens_docs
        , {{dernier_evt('du1','nomproc')}} as type_evt
        , du1.datappro as date_evt
        , null as date_creation
        , du1.datefin as date_expiration
        , 'DU' as type_doc
        , null as contact
        , du_id.grid_name as codegeo
    from du_id
    left join (
        select distinct on(du.datappro,du.partition) 
            du.* 
        from du
        inner join (
            select distinct on(du.partition) 
                du.partition
                , du.datappro
            from (
                select 
                    MAX(TO_DATE(datappro,'YYYYMMDD')) as datappro
                    , partition
                from du 
                group by partition
            ) t1
            inner join du 
            on (
                TO_DATE(du.datappro,'YYYYMMDD') = t1.datappro 
                and du.partition = t1.partition
            )
        ) t2 
        on (
            t2.partition = du.partition 
            and t2.datappro = du.datappro
        )
    ) du1
    on du1.partition = du_id.partition
)

select * from du_infos
