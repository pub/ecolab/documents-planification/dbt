with com_pdm as (
    select
        case
            when length(codgeo) = 4
                then concat('0', codgeo)
            else codgeo
        end as codegeo
        , concat(codgeo, ';-;', nom_court_de_l_aom, ' ', annee_d_approbation) as id
        , nom_court_de_l_aom as code
        , 'PDM' as type_doc
    from {{ source('brut', 'com_pdm') }}
)

, i_pdm as (
    select * from {{ ref('i_pdm') }}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

{{ changement_codegeo('com_pdm', 'codegeo', 'id', 'passage', 2023) }}

select distinct on(i.code_doc, m.new_codegeo)
    i.code_doc
    , m.new_codegeo as codegeo
    , 'PDM' as type_doc
from macro_com_pdm m
join i_pdm i
    on substr(m.id, position(';-;' in m.id) + 3) = i.nom_doc 
