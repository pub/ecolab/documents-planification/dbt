import psycopg2
import os

conn = psycopg2.connect(
    host=os.environ.get('POSTGRESQL_HOST'),
    port=os.environ.get('POSTGRESQL_PORT'),
    dbname=os.environ.get('POSTGRESQL_DB'),
    user=os.environ.get('POSTGRESQL_USER'),
    password=os.environ.get('POSTGRESQL_PASSWORD')
)

cursor = conn.cursor()

request = '''
DROP INDEX IF EXISTS idx_1;
DROP INDEX IF EXISTS idx_2;
DROP INDEX IF EXISTS idx_3;
DROP INDEX IF EXISTS idx_4;
DROP INDEX IF EXISTS idx_5;
DROP INDEX IF EXISTS idx_6;
DROP INDEX IF EXISTS idx_7;
DROP INDEX IF EXISTS idx_8;
DROP INDEX IF EXISTS idx_9;
-- Créer un index sur contours_docs (table) avec la colonne code_doc
CREATE INDEX idx_1 ON dbt_dev_marts.contours_docs (code_doc);

-- Créer un index sur docs_communes (table) avec la colonne code_doc
CREATE INDEX idx_2 ON dbt_dev_marts.docs_communes (code_doc);

-- Créer un index sur docs_communes (table) avec la colonne type_doc
CREATE INDEX idx_3 ON dbt_dev_marts.docs_communes (type_doc);

-- Créer un index sur infos_docs_normalise (table) avec la colonne code_doc
CREATE INDEX idx_4 ON dbt_dev_marts.infos_docs_normalise (code_doc);

-- Créer un index sur infos_docs_normalise (table) avec la colonne type_doc
CREATE INDEX idx_5 ON dbt_dev_marts.infos_docs_normalise (type_doc);

-- Créer un index sur liste_ref (table) avec la colonne codegeo
CREATE INDEX idx_6 ON dbt_dev_marts.liste_ref (codegeo);

-- Créer un index sur liste_ref (table) avec la colonne dep
CREATE INDEX idx_7 ON dbt_dev_marts.liste_ref (dep);

-- Créer un index sur liste_ref (table) avec la colonne epci
CREATE INDEX idx_8 ON dbt_dev_marts.liste_ref (epci);

-- Créer un index sur liste_ref (table) avec la colonne reg
CREATE INDEX idx_9 ON dbt_dev_marts.liste_ref (reg);
'''

cursor.execute(request)
conn.commit()
cursor.close()
conn.close()

