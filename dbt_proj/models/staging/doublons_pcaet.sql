with siren_pcaet as(
    select * from {{ ref('siren_pcaet')}}
)

, matching as (
    select * from {{ ref('dgec_ademe_pcaet') }}
)

, donnees_ademe_temp as (
    select * from {{ source('brut', 'demarches_pcaet_v1') }}
    union
    select * from {{ source('brut', 'demarches_pcaet_v2') }}
)

, ademe_sans_doublons as (
    select max(id::int)::text as id from donnees_ademe_temp
    group by siren_collectivites_coporteuses
    having count(*)>1
    union
    select max(id::int)::text as id from donnees_ademe_temp
    group by siren_collectivites_coporteuses
    having count(*)=1
    union
    select id from donnees_ademe_temp
    where siren_collectivites_coporteuses is null
)

, donnees_ademe as(
    select t1.*
    from donnees_ademe_temp t1
    inner join ademe_sans_doublons
    using(id)
)

, doublons_siren as (
    select * from siren_pcaet
    join (
        select siren from siren_pcaet
        group by siren
        having(count(*)>1)
    ) t1
    using(siren)
)

, doublons_dans_ademe as (
    select *
    from doublons_siren
    where siren_structure_porteuse in (
        select siren_collectivites_coporteuses from donnees_ademe
    )
)

, siren_pcaet_sans_doublons as (
    select *
    from siren_pcaet
    where siren_structure_porteuse in (
        select siren_structure_porteuse from doublons_dans_ademe
    )
        or siren not in (
            select siren from doublons_siren
        )
    union
    select distinct on(siren) *
    from siren_pcaet
    where siren in (
        select siren from doublons_siren
    )
    and siren not in (
        select siren from doublons_dans_ademe
    )
)

select
    siren.*
    , matching.id
from siren_pcaet_sans_doublons siren
left join matching
using(siren)
