with brut_communes_crte_brut as (
    select 
        insee_com
        , id_crte
        , 'CRTE' as type_doc
    from {{source('brut', 'com_crte')}}
)

, i_crte as (
    select code_doc
    from {{ ref('i_crte' )}}
)

, brut_communes_crte_1 as (
    select
        insee_com as codegeo
        , replace(id_crte, ' ', '') as code_doc
        , concat(insee_com, ';-;', replace(id_crte, ' ', '')) as id
        , 'CRTE' as type_doc
    from brut_communes_crte_brut
    where id_crte not like '% ; %'
)

, brut_communes_crte_2 as (
    select
        insee_com as codegeo
        , replace(left(id_crte, position(' ; ' in id_crte) - 1), ' ', '') as code_doc
        , concat(insee_com, ';-;', replace(left(id_crte, position(' ; ' in id_crte) - 1), ' ', '')) as id
        , 'CRTE' as type_doc
    from brut_communes_crte_brut
    where id_crte like '% ; %'
)

, brut_communes_crte_3 as (
    select
        insee_com as codegeo
        , replace(substr(id_crte, position(' ; ' in id_crte) + 3, length(id_crte) - position(' ; ' in id_crte)), ' ', '') as code_doc
        , concat(insee_com, ';-;', replace(substr(id_crte, position(' ; ' in id_crte) + 3, length(id_crte)), ' ', '')) as id
        , 'CRTE' as type_doc
    from brut_communes_crte_brut
    where id_crte like '% ; %'
)

, brut_communes_crte as (
    select * from brut_communes_crte_1
    union
    select * from brut_communes_crte_2
    union
    select * from brut_communes_crte_3
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

{{ changement_codegeo('brut_communes_crte', 'codegeo', 'id', 'passage', 2023) }}


select distinct on(code_doc, codegeo)
    i.code_doc as code_doc
    , m.new_codegeo as codegeo
    , 'CRTE' as type_doc
from macro_brut_communes_crte m
join i_crte i
on substr(m.id, position(';-;' in m.id) + 3) = i.code_doc