with communes_docs as (
    select * from {{ ref('com_pdm') }}
)

, contour_communes as (
    select * from {{ ref('ref_communes_pour_fusion_geom') }}
)

, contours_docs_brut as (
    select
        cd.code_doc
        , type_doc
        , ST_UNION(cc.geom) as geom
    from communes_docs cd
    join contour_communes cc
    on cd.codegeo = cc.insee_com
    group by cd.code_doc, cd.type_doc
)

, contours_docs as (
    select 
        code_doc
        , type_doc
        , ST_Collect(ST_MakePolygon(geom)) as geom
    from (
        select code_doc, type_doc, ST_ExteriorRing((ST_Dump(geom)).geom) As geom
        from contours_docs_brut
    ) cd
    group by code_doc, type_doc
)

select * from contours_docs