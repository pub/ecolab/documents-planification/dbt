with liste_crte as (
    select * from {{ source('brut', 'crte_url') }}
)

, infos_crte as (
    select
        libelle_du_groupement as structure_porteuse
        , replace(id_crte, ' ', '') as code_doc
        , libelle_crte as nom_doc
        , {{ replace_etat('crte_signe_oui_non', "'CRTE'", 'protocole_d_accord_signe') }} as etat_document
        , case
            when protocole_d_accord_signe = 'Oui' then 'Protocole d''accord signé'
            when protocole_d_accord_signe = 'Non' then 'Protocole d''accord non signé'
            else protocole_d_accord_signe
        end as sous_etat
        , null as site_web
        , null as lien_centralise
        , CONCAT('CRTE : ',lien_contrat) as liens_docs
        , 'signature CRTE' as type_evt
        , date_previsionnelle_de_signature_du_crte as date_evt
        , date_previsionnelle_de_signature_du_crte as date_creation
        , null as date_expiration
        , 'CRTE' as type_doc
        , null as contact
        , commentaires as option_commentaires
    from liste_crte
)

select * from infos_crte