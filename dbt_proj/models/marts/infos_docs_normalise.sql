with sages as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_sage')}}
)

, sdages as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_sdage')}} 
)

, crte as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_crte')}} 
)

, scots as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_scot')}} 
)

, pcaet as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_pcaet')}} 
)

, plu as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_plu')}} 
)

, plui as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_plui')}} 
)

, cc as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_cc')}} 
)

, pdm as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_pdm')}} 
)

, sraddet as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        from {{ref('i_sraddet')}} 
)

, unions as (
    select * from scots
    union
    select * from crte
    union
    select * from sdages
    union
    select * from sages
    union
    select * from pcaet
    union
    select * from plu
    union
    select * from plui
    union
    select * from cc
    union
    select * from pdm
    union
    select * from sraddet
)

, unions_pop_superf as (
    select
        unions.*
        , pop.superficie
        , pop.pop
    from unions
    left join {{ ref('pop_superf_all') }} pop
    on pop.code_doc = unions.code_doc
)

, unions_bons_etats as (
    select 
        code_doc
        , type_doc
        , nom_doc
        , structure_porteuse
        , {{replace_etat('etat_document', 'type_doc', 'sous_etat')}} as etat_document
        , sous_etat
        , site_web
        , lien_centralise
        , liens_docs
        , type_evt
        , date_evt
        , date_creation
        , date_expiration
        , contact
        , superficie
        , pop  
        from unions_pop_superf
)

select * from unions_bons_etats