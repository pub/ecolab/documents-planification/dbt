FROM python:3.10

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1


COPY ./requirements-tests.txt requirements-tests.txt
COPY ./requirements.txt requirements.txt
COPY ./setup.py setup.py
COPY ./dbt_proj dbt_proj

RUN pip install -r requirements-tests.txt

RUN pip install --upgrade pip setuptools wheel \
    && pip install -e . \
    && pip cache purge \
    && rm -rf /root/.cache/pip

WORKDIR dbt_proj

RUN dbt deps --profiles-dir . --target prod

ENTRYPOINT ["dbt"]

CMD ["run", "-m", "+marts", "--profiles-dir", ".", "--target", "prod"]