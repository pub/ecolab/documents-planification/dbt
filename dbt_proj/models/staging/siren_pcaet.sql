with pcaet_dgec_ademe as (
    select
		case
			when nom_du_groupement = 'Commune de Paris'
				then '217500016'
			else siren
		end as siren
		, region_siege
		, departement_siege
		, nom_du_groupement
		, nature_juridique
		, groupement_interdepartemental
		, date_de_creation
		, date_d_effet
		, population
		, structure_porteuse
		, oblige_volontaire
		, lancement_demarche_0_1_
		, date__lancement
		, date_avis_ae
		, date_avis_etat
		, date_approbation
		, epci_concerne_par_article_85_de_la_lom
		, date_adoption_ou_maj_du_plan_d_action_qualite_de_l_air__paqa__d
		, scot_valant_pcaet
		, publication_territoires_et_climat_0_1_
	from {{ source('brut','pcaet_dgec_ademe') }}
)

, ref_collectivites_annees as (
    select * from {{ ref('ref_collectivites_annees') }}
)

, communes_dgec as (
	select
		t1.structure_porteuse
	, t2.insee
	from pcaet_dgec_ademe t1
	left join ref_collectivites_annees t2
		on t1.siren = t2.siren
	where t1.structure_porteuse is not null and t1.structure_porteuse != '0' and upper(t1.structure_porteuse) not like '%SCOT%' and t2.annee = '2023'
	order by structure_porteuse, insee
)

, communes_collectivites_2023 as (
	select * from ref_collectivites_annees
	where annee = '2023'
	order by siren, insee
)

, nom_collectivites_2023 as (
select distinct
	siren
	, replace(
		replace(
			replace(
				replace(
					upper(
						replace_special_characters(nom)),' ','')
				,'-','')
			,'’','')
		,'''','') as nom_norm
	from ref_collectivites_annees
	where annee = '2023'
	order by siren
)

, arrays_dgec as(
select
	array_agg(insee)
	, structure_porteuse
from communes_dgec
group by structure_porteuse
)

, array_communes_coll as (
	select
		array_agg(insee)
		, siren
	from communes_collectivites_2023
	group by siren
)

, matching_1 as (
select
	t1.siren
	, t2.structure_porteuse
from array_communes_coll t1
inner join arrays_dgec t2
using(array_agg)
)

, structures_porteuses as (
	select distinct on(structure_porteuse) * from pcaet_dgec_ademe
	where structure_porteuse is not null and structure_porteuse != '0' and upper(structure_porteuse) not like '%SCOT%'
)

, reliquats as (
select 
	t1.*
	, replace(
		replace(
			replace(
				replace(
					upper(
						replace_special_characters(t1.structure_porteuse)),' ','')
				,'-','')
			,'’','')
		,'''','') as nom_norm
from structures_porteuses t1
left join matching_1 t2
on t1.structure_porteuse = t2.structure_porteuse
where t2.structure_porteuse is null
)

, fusion_nom_reliquats as (
	select
		t1.siren
		, t2.structure_porteuse
	from nom_collectivites_2023 t1
	join reliquats t2
		on t1.nom_norm = t2.nom_norm
)

, reliquats_2 as (
	select t1.*
	from reliquats t1
	left join fusion_nom_reliquats t2
	on t1.structure_porteuse = t2.structure_porteuse
	where t2.structure_porteuse is null
)

, fusion_noms_reliquats_2 as (
	select
		t1.siren
		, t2.structure_porteuse
	from nom_collectivites_2023 t1
	join reliquats_2 t2
	on t1.nom_norm like concat('%', t2.nom_norm, '%') or t2.nom_norm like concat('%', t1.nom_norm, '%')
)

, scots_dgec as (
	select
		t1.*
		, t2.insee
	from pcaet_dgec_ademe t1
	left join ref_collectivites_annees t2
		on t1.siren = t2.siren  
	where upper(structure_porteuse) like '%SCOT%' and t2.annee = '2023'
	order by t1.siren, t2.insee
)

, communes_scots as (
	select *
	from {{ ref('com_scot') }}
	order by code_doc, codegeo
)

, arrays_scots_dgec as(
select
	array_agg(
		replace(
			replace(insee,'2A','000000')
		, '2B', '111111')::int)
	, siren
from scots_dgec
group by siren
)

, array_communes_scots as (
	select
		array_agg(
			replace(
				replace(codegeo,'2B','111111')
				, '2A', '000000')::int)
		, code_doc
	from communes_scots
	group by code_doc
)

, matching_scots as (
select
	t1.code_doc
	, t2.siren
from array_communes_scots t1
inner join arrays_scots_dgec t2
on t2.array_agg <@ t1.array_agg
)

, tous_les_pcaet_temp as(
    select * from matching_1
    union
    select * from fusion_nom_reliquats
    union
    select * from fusion_noms_reliquats_2
)

, pcaet_avec_siren as (
    select
        dgec.*
        , siren.siren as siren_structure_porteuse
    from pcaet_dgec_ademe dgec
    inner join tous_les_pcaet_temp siren
        on dgec.structure_porteuse = siren.structure_porteuse
    union
    select
		dgec.siren
		, dgec.region_siege
		, dgec.departement_siege
		, dgec.nom_du_groupement
		, dgec.nature_juridique
		, dgec.groupement_interdepartemental
		, dgec.date_de_creation
		, dgec.date_d_effet
		, dgec.population
		, infos.structure_porteuse as structure_porteuse
		, dgec.oblige_volontaire
		, dgec.lancement_demarche_0_1_
		, dgec.date__lancement
		, dgec.date_avis_ae
		, dgec.date_avis_etat
		, dgec.date_approbation
		, dgec.epci_concerne_par_article_85_de_la_lom
		, dgec.date_adoption_ou_maj_du_plan_d_action_qualite_de_l_air__paqa__d
		, dgec.scot_valant_pcaet
		, dgec.publication_territoires_et_climat_0_1_
		, right(scots.code_doc, 9) as siren_structure_porteuse
    from pcaet_dgec_ademe dgec
    inner join matching_scots scots
        on dgec.siren = scots.siren
	left join {{ ref('i_scot') }} infos
		on infos.code_doc = scots.code_doc
	union
	select
		siren
		, region_siege
		, departement_siege
		, nom_du_groupement
		, nature_juridique
		, groupement_interdepartemental
		, date_de_creation
		, date_d_effet
		, population
		, nom_du_groupement as structure_porteuse
		, oblige_volontaire
		, lancement_demarche_0_1_
		, date__lancement
		, date_avis_ae
		, date_avis_etat
		, date_approbation
		, epci_concerne_par_article_85_de_la_lom
		, date_adoption_ou_maj_du_plan_d_action_qualite_de_l_air__paqa__d
		, scot_valant_pcaet
		, publication_territoires_et_climat_0_1_
		, siren as siren_structure_porteuse
	from pcaet_dgec_ademe
	where structure_porteuse is null or structure_porteuse = '0'
)

, pcaet_all as (
	select * from pcaet_avec_siren
	union
	select
		dgec.*
		, null as siren_structure_porteuse
	from pcaet_dgec_ademe dgec
	left join pcaet_avec_siren siren
	on siren.siren = dgec.siren
	where siren.siren is null
)

select * from pcaet_all
