with pcaet_siren as (
    select * from {{ ref('doublons_pcaet') }}
)

, siren_codegeo as (
    select * from {{source('brut','communes_siren_2023')}}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

, collectivites as (
    select * from {{ref('ref_collectivites_non_epci')}}
)

, collectivites_annees as (
    select * from {{ref('ref_collectivites_annees')}}
)

, communes_pcaet_0 as (
     select
        c.insee
        , pcaet.siren as codegeo
        , pcaet.siren as code_doc
        , concat(c.insee,';-;', pcaet.siren) as id
        , 'PCAET' as type_doc
    from pcaet_siren pcaet
    left join collectivites c
    on c.siren = pcaet.siren
)

, communes_pcaet_2023 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_0 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2023'
    where pcaet.insee is null
)

, communes_pcaet_2022 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2023 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2022'
    where pcaet.insee is null
)

, communes_pcaet_2021 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2022 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2021'
    where pcaet.insee is null
)

, communes_pcaet_2020 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2022 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2020'
    where pcaet.insee is null
)

, communes_pcaet_2019 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2020 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2019'
    where pcaet.insee is null
)

, communes_pcaet_2018 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2019 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2018'
    where pcaet.insee is null
)

, communes_pcaet_2017 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2018 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2017'
    where pcaet.insee is null
)

, communes_pcaet_2016 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2017 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2016'
    where pcaet.insee is null
)

, communes_pcaet_2015 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2016 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2015'
    where pcaet.insee is null
)

, communes_pcaet_2014 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2015 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2014'
    where pcaet.insee is null
)

, communes_pcaet_2013 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2014 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2013'
    where pcaet.insee is null
)

, communes_pcaet_2012 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2013 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2012'
    where pcaet.insee is null
)

, communes_pcaet_2011 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2012 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2011'
    where pcaet.insee is null
)

, communes_pcaet_2010 as(
     select 
        c.insee
        , pcaet.codegeo
        , pcaet.code_doc
        , concat(c.insee,';-;', pcaet.code_doc) as id
        , 'PCAET' as type_doc
    from communes_pcaet_2011 pcaet
    left join collectivites_annees c
    on pcaet.codegeo = c.siren and c.annee = '2010'
    where pcaet.insee is null
)

{{ changement_codegeo_annee_connue('2023' , 'communes_pcaet_0', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2023' , 'communes_pcaet_2023', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2022' , 'communes_pcaet_2022', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2021' , 'communes_pcaet_2021', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2020' , 'communes_pcaet_2020', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2019' , 'communes_pcaet_2019', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2018' , 'communes_pcaet_2018', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2017' , 'communes_pcaet_2017', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2016' , 'communes_pcaet_2016', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2015' , 'communes_pcaet_2015', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2014' , 'communes_pcaet_2014', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2013' , 'communes_pcaet_2013', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2012' , 'communes_pcaet_2012', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2011' , 'communes_pcaet_2011', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2010' , 'communes_pcaet_2010', 'insee', 'id', 'passage', '2023') }}

, fusion_macro as (
    select * from macro_communes_pcaet_0
    union
    select * from macro_communes_pcaet_2023
    union
    select * from macro_communes_pcaet_2022
    union
    select * from macro_communes_pcaet_2021
    union
    select * from macro_communes_pcaet_2020
    union
    select * from macro_communes_pcaet_2019
    union
    select * from macro_communes_pcaet_2018
    union
    select * from macro_communes_pcaet_2017
    union
    select * from macro_communes_pcaet_2016
    union
    select * from macro_communes_pcaet_2015
    union
    select * from macro_communes_pcaet_2014
    union
    select * from macro_communes_pcaet_2013
    union
    select * from macro_communes_pcaet_2012
    union
    select * from macro_communes_pcaet_2011
    union
    select * from macro_communes_pcaet_2010
)

, com_pcaet as (
    select
        concat('PCAET_'
            , replace(
                replace(
                    replace(
                        replace(
                            lower(
                                replace_special_characters(pcaet.structure_porteuse)
                                )
                        ,' ','')
                    ,'-','')
                ,'’','')
            ,'''','')
        ) as code_doc
        , f.new_codegeo as codegeo
        , 'PCAET' as type_doc
    from pcaet_siren pcaet
    join fusion_macro f
    on substr(f.id, position(';-;' in f.id) + 3) = pcaet.siren
    union
    select
        concat('PCAET_'
            , replace(
                replace(
                    replace(
                        replace(
                            lower(
                                replace_special_characters(pcaet.structure_porteuse)
                                )
                        ,' ','')
                    ,'-','')
                ,'’','')
            ,'''','')
        ) as code_doc
        , sc.insee as codegeo
        , 'PCAET' as type_doc
    from pcaet_siren pcaet
    join siren_codegeo sc
        using(siren)
)

select distinct on(code_doc, codegeo) * from com_pcaet