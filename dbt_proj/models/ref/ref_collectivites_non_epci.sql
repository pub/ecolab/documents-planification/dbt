with collectivites as (
    select * from {{source('brut','collectivites_all')}}
)

, communes_collectivites as (
    select * from collectivites
    where type_membre = 'Commune'
    union
    select 
        t1.dept
        ,t1.siren
        ,t1.raison_sociale
        ,t1.nature_juridique
        ,t1.mode_financ
        ,t1.arret_competences
        ,t2.siren_membre
        ,t2.nom_membre
        ,t2.type_membre
    from collectivites t1
    join collectivites t2 on t1.siren_membre = t2.siren
)

, siren_codegeo as (
    select * from {{source('brut','communes_siren')}}
)

, communes_collectivites_final as (
    select 
        t1.dept
        ,t1.siren
        ,t1.raison_sociale
        ,t1.nature_juridique
        ,t1.mode_financ
        ,t1.arret_competences
        ,t2.insee
        ,t1.nom_membre
        ,t1.type_membre
    from communes_collectivites t1
    join siren_codegeo t2 on t1.siren_membre = t2.siren
)

select * from communes_collectivites_final