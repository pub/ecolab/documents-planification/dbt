with contours as (
    select * from {{ ref('c_all')}}
)

, contours_geojson as (
    select
        code_doc
        , type_doc
        , ST_ASGeoJSON(geom) as geom
    from contours
)

select * from contours_geojson