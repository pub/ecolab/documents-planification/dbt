with sraddet as (
    select * from {{source('brut','sraddet')}}
)

, infos_sraddet as (
    select
        structure_porteuse as structure_porteuse
        , concat('SRADDET_', insee) as code_doc
        , nom_du_doc as nom_doc
        , etat_document as etat_document
        , null as sous_etat
        , site_web as site_web
        , lien_doc as lien_centralise
        , null as liens_docs
        , null as type_evt
        , null as date_evt
        , null as date_creation
        , null as date_expiration
        , 'SRADDET' as type_doc
        , 'null' as contact
        from sraddet
)

select * from infos_sraddet
