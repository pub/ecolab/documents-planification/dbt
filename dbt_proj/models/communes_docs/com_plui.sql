with brut_grid_du as (
    select 
        codegeo
        , code_doc as code_doc
        , 'PLUI' as type_doc
    from {{ref('i_plui')}}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

, collectivites as (
    select * from {{ref('ref_collectivites_non_epci')}}
)

, collectivites_annees as (
    select * from {{ref('ref_collectivites_annees')}}
)

, brut_communes_du as (
    select
        *
        , concat(codegeo, ';-;', code_doc) as id
    from brut_grid_du
    where length(codegeo) = 5
)

, brut_siren_du as (
    select *
    from brut_grid_du
    where length(codegeo) != 5
)

, communes_du_0 as (
     select
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from brut_siren_du du
    left join collectivites c
    on c.siren = du.codegeo
)

, communes_du_2023 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_0 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2023'
    where du.insee is null
)

, communes_du_2022 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2023 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2022'
    where du.insee is null
)

, communes_du_2021 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2022 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2021'
    where du.insee is null
)

, communes_du_2020 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2022 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2020'
    where du.insee is null
)

, communes_du_2019 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2020 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2019'
    where du.insee is null
)

, communes_du_2018 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2019 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2018'
    where du.insee is null
)

, communes_du_2017 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2018 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2017'
    where du.insee is null
)

, communes_du_2016 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2017 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2016'
    where du.insee is null
)

, communes_du_2015 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2016 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2015'
    where du.insee is null
)

, communes_du_2014 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2015 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2014'
    where du.insee is null
)

, communes_du_2013 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2014 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2013'
    where du.insee is null
)

, communes_du_2012 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2013 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2012'
    where du.insee is null
)

, communes_du_2011 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2012 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2011'
    where du.insee is null
)

, communes_du_2010 as(
     select 
        c.insee
        , du.codegeo
        , du.code_doc
        , concat(c.insee,';-;', du.code_doc) as id
        , 'PLUI' as type_doc
    from communes_du_2011 du
    left join collectivites_annees c
    on du.codegeo = c.siren and c.annee = '2010'
    where du.insee is null
)

{{ changement_codegeo('brut_communes_du', 'codegeo', 'id', 'passage', 2023) }}
{{ changement_codegeo_annee_connue('2023' , 'communes_du_0', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2023' , 'communes_du_2023', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2022' , 'communes_du_2022', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2021' , 'communes_du_2021', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2020' , 'communes_du_2020', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2019' , 'communes_du_2019', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2018' , 'communes_du_2018', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2017' , 'communes_du_2017', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2016' , 'communes_du_2016', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2015' , 'communes_du_2015', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2014' , 'communes_du_2014', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2013' , 'communes_du_2013', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2012' , 'communes_du_2012', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2011' , 'communes_du_2011', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2010' , 'communes_du_2010', 'insee', 'id', 'passage', '2023') }}

, fusion_macro as (
    select * from macro_brut_communes_du
    union
    select * from macro_communes_du_0
    union
    select * from macro_communes_du_2023
    union
    select * from macro_communes_du_2022
    union
    select * from macro_communes_du_2021
    union
    select * from macro_communes_du_2020
    union
    select * from macro_communes_du_2019
    union
    select * from macro_communes_du_2018
    union
    select * from macro_communes_du_2017
    union
    select * from macro_communes_du_2016
    union
    select * from macro_communes_du_2015
    union
    select * from macro_communes_du_2014
    union
    select * from macro_communes_du_2013
    union
    select * from macro_communes_du_2012
    union
    select * from macro_communes_du_2011
    union
    select * from macro_communes_du_2010
)

, communes_du as (
    select
        du.code_doc
        , f.new_codegeo as codegeo
        , 'PLUI' as type_doc
    from brut_grid_du du
    join fusion_macro f
    on substr(f.id, position(';-;' in f.id) + 3) = du.code_doc
)

select * from communes_du
