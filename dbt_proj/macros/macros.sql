{% macro replace_etat(etat_document, type_doc, sous_etat) %}
replace({{etat_document}}, {{etat_document}},
	case 
		when {{etat_document}} is null then 'Etat inconnu'
		when {{etat_document}} = 'Élaboration' then 'En élaboration'
        when {{etat_document}} = 'Signé' then 'Signé'
		when {{etat_document}} = 'En élaboration' then 'En élaboration'
		when {{etat_document}} = 'Émergence' then 'En élaboration'
		when {{etat_document}} = '01' then 'En élaboration'
		when {{etat_document}} = '02' then 'Non démarré'
		when {{etat_document}} = '03' then 'Opposable'
		when {{etat_document}} = '04' then 'Annulé'
        when {{etat_document}} = '05' then 'Remplacé'
        when {{etat_document}} = '06' then 'Annulé'
        when {{etat_document}} = '07' then 'Approuvé'
        when {{etat_document}} = '08' then 'Partiellement annulé'
        when {{etat_document}} = '09' then 'Annulé'
        when {{etat_document}} = 'true' and {{type_doc}} = 'CRTE' then 'Signé'
        when {{etat_document}} = 'true' then 'Approuvé'
        when {{etat_document}} = 'Abandon' then 'Abandonné'
        when {{etat_document}} = 'Non démarré' then 'Non démarré'
        when {{etat_document}} = 'Instruction' then 'Non démarré'
        when {{etat_document}} = 'Mis en œuvre' then 'Opposable'
        when {{etat_document}} = 'false' and {{sous_etat}} = 'oui' then 'En élaboration'
        when {{etat_document}} =  'false' then 'Non démarré'
        when {{etat_document}} = 'Validé' then 'Opposable'
        when {{etat_document}} = 'Opposable' then 'Opposable'
        when {{etat_document}} = 'En élaboration' then 'En élaboration'
        when {{etat_document}} = 'Non démarré' then 'Non démarré'
        when {{etat_document}} = 'Adopté' then 'Adopté'
        when {{etat_document}} = 'Annulé' then 'Annulé'
        when {{etat_document}} = 'Remplacé' then 'Remplacé'
        when {{etat_document}} = 'Approuvé' then 'Approuvé'
        when {{etat_document}} = 'Partiellement annulé' then 'Partiellement annulé'
        else 'Etat inconnu'
    end
)
{% endmacro %}


{% macro collectivites_communes(annee) %}
    select * from collectivites
    where type_membre = 'Commune' and annee = CONCAT({{annee}},'')
    union
    select 
        t1.siren
        , t1.nom
        , t2.type_membre
        , t2.siren_membre
        , t1.adhesion_siren
        , t1.annee
    from collectivites t1
    join collectivites t2 on t1.siren_membre = t2.siren
    where t1.annee = CONCAT({{annee}},'') and t2.annee = CONCAT({{annee}},'') 
{% endmacro %}


{% macro replace_sous_etat() %}
replace(etat_document,etat_document,
	case 
		when sous_etat is null then null

		when sous_etat = '01' then 'En élaboration'
		when sous_etat = '02' then 'Non démarré'
		when sous_etat = '03' then 'Opposable'
		when sous_etat = '04' then 'Annulé'
        when sous_etat = '05' then 'Remplacé'
        when sous_etat = '06' then 'Annulé'
        when sous_etat = '07' then 'Fin de la procédure'
        when sous_etat = '08' then 'Partiellement annulé'
        when sous_etat = '09' then 'Annulé'
        when sous_etat = 'true' then 'Opposable'
        when sous_etat =  'Abandon' then 'Abandonné'
        when sous_etat = 'Non démarré' then 'Non démarré'
        when sous_etat = 'Instruction' then 'Non démarré'
        when sous_etat = 'Mis en œuvre' then 'Opposable'
        when sous_etat = 'false' and sous_etat = 'oui' then 'En élaboration'
        when sous_etat =  'false' then 'Non démarré'
        when sous_etat = 'Validé' then 'Opposable'
    end
)
{% endmacro %}

{% macro dernier_evt(table, colonne_proc) %}
replace({{table}}.{{colonne_proc}}, {{table}}.{{colonne_proc}},
	case 
		when {{table}}.{{colonne_proc}} is null then null
		when {{table}}.{{colonne_proc}} = '01' then 'Élaboration'
		when {{table}}.{{colonne_proc}} = '02' then 'Révision'
		when {{table}}.{{colonne_proc}} = '03' then 'Révision simplifiée'
		when {{table}}.{{colonne_proc}} = '04' then 'Modification'
        when {{table}}.{{colonne_proc}} = '05' then 'Modification simplifiée'
        when {{table}}.{{colonne_proc}} = '06' then 'Mise en compatibilité'
        when {{table}}.{{colonne_proc}} = 'E' then 'Élaboration'
        when left({{table}}.{{colonne_proc}}, 2) = 'MC' then CONCAT('Mise en compatibilité n°', SUBSTR({{table}}.{{colonne_proc}}, 3))
        when left({{table}}.{{colonne_proc}}, 2) = 'MJ' then CONCAT('Mise à jour des annexes n°', SUBSTR({{table}}.{{colonne_proc}}, 3))
        when left({{table}}.{{colonne_proc}}, 2) = 'MS' then CONCAT('Modification simplifiée n°', SUBSTR({{table}}.{{colonne_proc}}, 3))
        when left({{table}}.{{colonne_proc}}, 1) =  'M' then CONCAT('Modification de droit commun n°', SUBSTR({{table}}.{{colonne_proc}}, 2))
        when left({{table}}.{{colonne_proc}}, 2) = 'RA' then 'Révision effectuée en application de l''article L. 153-34 du code de l''urbanisme'
        when left({{table}}.{{colonne_proc}}, 2) = 'RS' then CONCAT('Révision simplifiée n°', SUBSTR({{table}}.{{colonne_proc}}, 3))
        when {{table}}.{{colonne_proc}} = 'R' then 'Révision'
        when {{table}}.{{colonne_proc}} = 'A' then 'Abrogation'
        else {{table}}.{{colonne_proc}}
    end
)
{% endmacro %}

{% macro changement_codegeo(table1, codegeo, id, table_passage, annee_cible) %}

    , {{table1}}macro_nombre_correspondance_0 as (
        select
            passage.annee
            , passage.codegeo
        from {{table1}} t1
        join {{table_passage}} passage
        on passage.codegeo = t1.{{ codegeo }}
    )

    , {{table1}}macro_nombre_correspondance as (
        select
            annee
            , count(*) as nb_join
        from {{table1}}macro_nombre_correspondance_0
        group by annee
    )

    , {{table1}}macro_annee as (
        select annee
        from {{table1}}macro_nombre_correspondance
        where nb_join in (
            select max(nb_join) from {{table1}}macro_nombre_correspondance
        )
        order by annee::int desc
        limit 1
    )
    , {{table1}}_avec_annee as (
        select 
            {{table1}}.{{id}}
            , {{table1}}.{{codegeo}}
            , {{table1}}macro_annee.annee as geo_annee
        from {{table1}} cross join {{table1}}macro_annee
    )

    , {{table1}}macro_table_modif as (
        select
            t_annee.{{id}}
            , t_annee.{{codegeo}}
            , p.codegeo_cible
            as new_codegeo
        from {{table1}}_avec_annee t_annee
        left join {{table_passage}} p
        on p.annee = t_annee.geo_annee and t_annee.{{codegeo}} = p.codegeo)

, {{table1}}reliquats_0 as (
	select * from {{table1}}macro_table_modif
where new_codegeo is null
)
	, {{table1}}reliquats as (
	select
		t_annee.{{id}}
		, p.codegeo_cible as new_codegeo_2
        , p.annee as test_annee
	from {{table1}}reliquats_0 t_annee
	left join {{table_passage}} p
	        on
        case
            when (p.annee = 2023 and t_annee.{{codegeo}} = p.codegeo) then (p.annee = 2023 and t_annee.{{codegeo}} = p.codegeo)
            when p.annee = 2022 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2022 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2021 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2021 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2020 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2020 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2019 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2019 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2018 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2018 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2017 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2017 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2016 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2016 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2015 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2015 and t_annee.{{codegeo}} = p.codegeo
        end
    )

    , {{table1}}reliquats_annee as (
        select
            {{id}}
            , max(test_annee) as annee
        from {{table1}}reliquats
        group by {{id}}
    )

    , {{table1}}reliquats_final as (
        select
            re.{{id}}
            , new_codegeo_2 as codegeo_new
        from {{table1}}reliquats re
        join {{table1}}reliquats_annee ra
        on re.{{id}} = ra.{{id}} and re.test_annee = ra.annee
    )

    , macro_{{table1}} as(
        select
            {{id}}
            , new_codegeo
        from {{table1}}macro_table_modif
        where new_codegeo is not null
        union
        select
            {{id}}
            , codegeo_new
        from {{table1}}reliquats_final
    )

{% endmacro %}

{% macro changement_codegeo_annee_connue(annee, table1, codegeo, id, table_passage, annee_cible) %}

    , {{table1}}_avec_annee as (
        select 
            {{table1}}.{{id}}
            , {{table1}}.{{codegeo}}
            , cast({{annee}} as int) as geo_annee
        from {{table1}}
    )

    , {{table1}}macro_table_modif as (
        select
            t_annee.{{id}}
            , t_annee.{{codegeo}}
            , p.codegeo_cible
            as new_codegeo
        from {{table1}}_avec_annee t_annee
        left join {{table_passage}} p
        on p.annee = t_annee.geo_annee and t_annee.{{codegeo}} = p.codegeo)

, {{table1}}reliquats_0 as (
	select * from {{table1}}macro_table_modif
where new_codegeo is null
)
	, {{table1}}reliquats as (
	select
		t_annee.{{id}}
		, p.codegeo_cible as new_codegeo_2
        , p.annee as test_annee
	from {{table1}}reliquats_0 t_annee
	left join {{table_passage}} p
	        on
        case
            when (p.annee = 2023 and t_annee.{{codegeo}} = p.codegeo) then (p.annee = 2023 and t_annee.{{codegeo}} = p.codegeo)
            when p.annee = 2022 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2022 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2021 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2021 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2020 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2020 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2019 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2019 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2018 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2018 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2017 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2017 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2016 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2016 and t_annee.{{codegeo}} = p.codegeo
            when p.annee = 2015 and t_annee.{{codegeo}} = p.codegeo then p.annee = 2015 and t_annee.{{codegeo}} = p.codegeo
        end
    )

    , {{table1}}reliquats_annee as (
        select
            {{id}}
            , max(test_annee) as annee
        from {{table1}}reliquats
        group by {{id}}
    )

    , {{table1}}reliquats_final as (
        select
            re.{{id}}
            , new_codegeo_2 as codegeo_new
        from {{table1}}reliquats re
        join {{table1}}reliquats_annee ra
        on re.{{id}} = ra.{{id}} and re.test_annee = ra.annee
    )

    , macro_{{table1}} as(
        select
            {{id}}
            , new_codegeo
        from {{table1}}macro_table_modif
        where new_codegeo is not null
        union
        select
            {{id}}
            , codegeo_new as new_codegeo
        from {{table1}}reliquats_final
    )

{% endmacro %}

{% macro macro_communes_array(table1, jointure_col, jointure_table, collectivites, annee) %}


    , macro_communes_2023{{ table1 }}{{ jointure_table }} as (
        select
            t1.{{ jointure_table }}
            , p.insee
            , '2023' as annee

        from {{ table1 }} t1
        inner join {{ collectivites }} p
	        on (p.annee = '2023' and t1.{{jointure_table}} = p.{{ jointure_col }})
    )
    , macro_communes_2023_reliquats{{ table1 }}{{ jointure_table }} as (
        select t1.{{ jointure_table }}
        from {{ table1 }} t1
        left join macro_communes_2023{{ table1 }}{{ jointure_table }} com
            on t1.{{ jointure_table }} = com.{{ jointure_table }}
        where com.{{ jointure_table }} is null
    )

    , macro_communes_annees{{ table1 }}{{ jointure_table }} as (
        select
            t1.{{ jointure_table }}
            , p.insee
            , p.annee
        from macro_communes_2023_reliquats{{ table1 }}{{ jointure_table }} t1
        left join {{ collectivites }} p
	        on
            case
                when p.annee = '2022' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2022' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2021' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2021' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2020' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2020' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2019' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2019' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2018' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2018' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2017' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2017' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2016' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2016' and t1.{{jointure_table}} = p.{{ jointure_col }}
                when p.annee = '2015' and t1.{{jointure_table}} = p.{{ jointure_col }} then p.annee = '2015' and t1.{{jointure_table}} = p.{{ jointure_col }}
            end
    )
    
    , macro_{{table1}}{{ jointure_table }}communestemp as (
        select
            {{ jointure_table }}
            , max(annee) as annee
        from macro_communes_annees{{ table1 }}{{ jointure_table }}
        group by {{ jointure_table }}
    )

    , macro_{{table1}}{{ jointure_table }}_communes_all as (
        select * from macro_communes_2023{{ table1 }}{{ jointure_table }}
        union
        select t1.*
        from macro_communes_annees{{ table1 }}{{ jointure_table }} t1
        join macro_{{table1}}{{ jointure_table }}communestemp t2
            on t1.annee = t2.annee and t1.{{ jointure_table }} = t2.{{ jointure_table }}
        order by {{ jointure_table }}, insee
    )
{% endmacro %}

{% macro matching_collectivites(table1, col_table_1, table2, col_table_2) %}
    , macro_array_{{table1}} as (
        select
            array_agg(insee)
            , {{ col_table_1 }}
        from {{ table1 }}
        group by {{ col_table_1 }}
    )

    , macro_array_{{table2}} as (
        select
            array_agg(insee)
            , {{ col_table_2 }}
        from {{ table2 }}
        group by {{ col_table_2 }}
    )

    , macro_{{table1}}_{{table2}} as (
        select
            t1.{{ col_table_1 }}
            , t2.{{ col_table_2 }}
        from macro_array_{{table1}} t1
        join macro_array_{{table2}} t2
        using(array_agg)
    )

{% endmacro %}
