with infos_scots as (
    select
        code_doc
    from {{ref('i_scot')}}
)

, collectivites as (
    select * from {{ref('ref_collectivites_non_epci')}}
)

, collectivites_annees as (
    select * from {{ref('ref_collectivites_annees')}}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

, communes_scot_0 as (
    select
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from infos_scots s
    left join collectivites c
    on c.siren = right(s.code_doc,9)
)

, communes_scot_2023 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_0 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2023'
    where s.insee is null
)

, communes_scot_2022 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2023 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2022'
    where s.insee is null
)

, communes_scot_2021 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2022 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2021'
    where s.insee is null
)

, communes_scot_2020 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2022 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2020'
    where s.insee is null
)

, communes_scot_2019 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2020 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2019'
    where s.insee is null
)

, communes_scot_2018 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2019 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2018'
    where s.insee is null
)

, communes_scot_2017 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2018 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2017'
    where s.insee is null
)

, communes_scot_2016 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2017 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2016'
    where s.insee is null
)

, communes_scot_2015 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2016 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2015'
    where s.insee is null
)

, communes_scot_2014 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2015 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2014'
    where s.insee is null
)

, communes_scot_2013 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2014 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2013'
    where s.insee is null
)

, communes_scot_2012 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2013 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2012'
    where s.insee is null
)

, communes_scot_2011 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2012 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2011'
    where s.insee is null
)

, communes_scot_2010 as(
    select 
        c.insee
        , s.code_doc
        , concat(c.insee,';-;', s.code_doc) as id
        , 'SCoT' as type_doc
    from communes_scot_2011 s
    left join collectivites_annees c
    on right(s.code_doc,9) = c.siren and c.annee = '2010'
    where s.insee is null
)

{{ changement_codegeo_annee_connue('2023' , 'communes_scot_0', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2023' , 'communes_scot_2023', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2022' , 'communes_scot_2022', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2021' , 'communes_scot_2021', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2020' , 'communes_scot_2020', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2019' , 'communes_scot_2019', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2018' , 'communes_scot_2018', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2017' , 'communes_scot_2017', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2016' , 'communes_scot_2016', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2015' , 'communes_scot_2015', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2014' , 'communes_scot_2014', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2013' , 'communes_scot_2013', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2012' , 'communes_scot_2012', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2011' , 'communes_scot_2011', 'insee', 'id', 'passage', '2023') }}
{{ changement_codegeo_annee_connue('2010' , 'communes_scot_2010', 'insee', 'id', 'passage', '2023') }}

, fusion_macro as (
    select * from macro_communes_scot_0
    union
    select * from macro_communes_scot_2023
    union
    select * from macro_communes_scot_2022
    union
    select * from macro_communes_scot_2021
    union
    select * from macro_communes_scot_2020
    union
    select * from macro_communes_scot_2019
    union
    select * from macro_communes_scot_2018
    union
    select * from macro_communes_scot_2017
    union
    select * from macro_communes_scot_2016
    union
    select * from macro_communes_scot_2015
    union
    select * from macro_communes_scot_2014
    union
    select * from macro_communes_scot_2013
    union
    select * from macro_communes_scot_2012
    union
    select * from macro_communes_scot_2011
    union
    select * from macro_communes_scot_2010
)

, communes_scot as (
    select
        s.code_doc
        , f.new_codegeo as codegeo
        , 'SCoT' as type_doc
    from infos_scots s
    join fusion_macro f
    on substr(f.id, position(';-;' in f.id) + 3) = s.code_doc
)

select * from communes_scot
