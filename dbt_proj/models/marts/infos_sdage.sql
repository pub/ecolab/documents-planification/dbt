select
    i.*
    , pop.superficie
    , pop.pop
from {{ ref('i_sdage') }} i
left join {{ ref('pop_superf_all') }} pop
on pop.code_doc = i.code_doc