with collectivites as (
    select * from {{source('brut','collectivites_annees')}}
)

, communes_collectivites as (
    {{collectivites_communes('2023')}}
    union
    {{collectivites_communes('2022')}}
    union
    {{collectivites_communes('2021')}}
    union
    {{collectivites_communes('2020')}}
    union
    {{collectivites_communes('2019')}}
    union
    {{collectivites_communes('2018')}}
    union
    {{collectivites_communes('2017')}}
    union
    {{collectivites_communes('2016')}}
    union
    {{collectivites_communes('2015')}}
    union
    {{collectivites_communes('2014')}}
    union
    {{collectivites_communes('2013')}}
    union
    {{collectivites_communes('2012')}}
    union
    {{collectivites_communes('2011')}}
    union
    {{collectivites_communes('2010')}}
)

, siren_codegeo as (
    select * from {{source('brut','communes_siren')}}
)

, siren_codegeo_annee as (
    select 
        insee
        , siren
        , '2015' as annee
    from {{source('brut','communes_siren_2015')}}
    union
    select 
        insee
        , siren
        , '2016' as annee
    from {{source('brut','communes_siren_2016')}}
    union
    select 
        insee
        , siren
        , '2016' as annee
    from {{source('brut','communes_siren_2016')}}
    union
    select 
        insee
        , siren
        , '2017' as annee
    from {{source('brut','communes_siren_2017')}}
    union
    select 
        insee
        , siren
        , '2018' as annee
    from {{source('brut','communes_siren_2018')}}
    union
    select 
        insee
        , siren
        , '2019' as annee
    from {{source('brut','communes_siren_2019')}}
    union
    select 
        insee
        , siren
        , '2020' as annee
    from {{source('brut','communes_siren_2020')}}
    union
    select 
        insee
        , siren
        , '2021' as annee
    from {{source('brut','communes_siren_2021')}}
    union
    select 
        insee
        , siren
        , '2022' as annee
    from {{source('brut','communes_siren_2022')}}
    union
    select 
        insee
        , siren
        , '2023' as annee
    from {{source('brut','communes_siren_2023')}}
)

, communes_collectivites_final as (
    select 
        t1.siren
        , t1.nom
        , t1.type_membre
        , t2.insee
        , t1.annee
    from communes_collectivites t1
    join siren_codegeo_annee t2 on t1.siren_membre = t2.siren and t1.annee = t2.annee
)

select * from communes_collectivites_final