with geom_communes as (
    select * from {{ref('ref_communes_geom')}}
)

, geom_communes_simplifie as (
    select * from {{ ref('ref_communes_pour_fusion_geom') }}
)

, infos_communes as (
    select * from {{ref('ref_communes')}}
)

, infos_epcis as (
    select * from {{source('brut', 'com_epci_2023')}}
)

, infos_dep as (
    select * from {{source('brut', 'dep')}}
)

, infos_reg as (
    select * from {{source('brut', 'reg')}}
)

, contours_epcis_0 as (
    select
        t1.epci as codegeo
        , sum(t1.pop::bigint) as pop
        , sum(t1.superficie::float) as superf
        , ST_UNION(r.geom) as geom
    from infos_communes t1
    join geom_communes_simplifie r
    on t1.codegeo = r.insee_com
    group by t1.epci
)

, contours_epcis as (
    select 
        s.codegeo
        , max(s.pop) as pop
        , max(s.superf) as superf
        , ST_Collect(ST_MakePolygon(s.geom)) as geom
    from (
        select codegeo, pop, superf, ST_ExteriorRing((ST_Dump(geom)).geom) As geom
        from contours_epcis_0
    ) s
    group by s.codegeo
)

, contours_dep_0 as (
    select
        t1.dep as codegeo
        , sum(t1.pop::bigint) as pop
        , sum(t1.superficie::float) as superf
        , ST_UNION(r.geom) as geom
    from infos_communes t1
    join geom_communes_simplifie r
    on t1.codegeo = r.insee_com
    group by t1.dep
)

, contours_dep as (
    select 
        s.codegeo
        , max(s.pop) as pop
        , max(s.superf) as superf
        , ST_Collect(ST_MakePolygon(s.geom)) as geom
    from (
        select codegeo, pop, superf, ST_ExteriorRing((ST_Dump(geom)).geom) As geom
        from contours_dep_0
    ) s
    group by s.codegeo
)


, contours_reg_0 as (
    select
        t1.reg as codegeo
        , sum(t1.pop::bigint) as pop
        , sum(t1.superficie::float) as superf
        , ST_UNION(r.geom) as geom
    from infos_communes t1
    join geom_communes_simplifie r
    on t1.codegeo = r.insee_com
    group by t1.reg
)

, contours_reg as (
    select 
        s.codegeo
        , max(s.pop) as pop
        , max(s.superf) as superf
        , ST_Collect(ST_MakePolygon(s.geom)) as geom
    from (
        select codegeo, pop, superf, ST_ExteriorRing((ST_Dump(geom)).geom) As geom
        from contours_reg_0
    ) s
    group by s.codegeo
)

, infos_geom as (
    select
        t1.codegeo
        , t1.epci
        , t1.dep
        , t1.reg
        , t1.pop
        , t1.superficie as superf
        , t2.nom_m as nom
        , t2.geom
        , 'commune' as type_geo
        , t2.longitude
        , t2.latitude
    from infos_communes t1
    join geom_communes t2
    on t1.codegeo = t2.codegeo
    union
    select
        t1.codegeo
        , t1.codegeo as epci
        , t2.dept as dep
        , dep.reg as reg
        , t1.pop
        , t1.superf
        , t2.raison_sociale as nom
        , t1.geom
        , 'epci' as type_geo
        , ST_X(ST_Centroid(t1.geom)) as longitude
        , ST_Y(ST_Centroid(t1.geom)) as latitude
    from contours_epcis t1
    join infos_epcis t2
        on t1.codegeo = t2.siren
    join infos_dep dep
        on dep.dep = t2.dept
        union
    select
        t1.codegeo
        , null as epci
        , t1.codegeo as dep
        , dep.reg as reg
        , t1.pop
        , t1.superf
        , dep.ncc as nom
        , t1.geom
        , 'dep' as type_geo
        , ST_X(ST_Centroid(t1.geom)) as longitude
        , ST_Y(ST_Centroid(t1.geom)) as latitude
    from contours_dep t1
    join infos_dep dep
        on t1.codegeo = dep.dep
        union
    select
        t1.codegeo
        , null as epci
        , null as dep
        , t1.codegeo as reg
        , t1.pop
        , t1.superf
        , reg.ncc as nom
        , t1.geom
        , 'reg' as type_geo
        , ST_X(ST_Centroid(t1.geom)) as longitude
        , ST_Y(ST_Centroid(t1.geom)) as latitude
    from contours_reg t1
    join infos_reg reg
        on t1.codegeo = reg.reg
)

select * from infos_geom
