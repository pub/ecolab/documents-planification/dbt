# Documents de planification - dbt

## Installation

Inspired from [dbt docs](https://docs.getdbt.com/tutorial/create-a-project-dbt-cli) and [dgaln template](https://gitlab-forge.din.developpement-durable.gouv.fr/dgaln/mission-numerique/dbt-template/-/blob/main/README.md#modal-confirm-fork-webide)

### 1. Install a code editor locally
On windows, we recommand VS code
1) Follow [the instructions](https://code.visualstudio.com/download) to download and install VS code

### 2. Install git

- Follow the instructions to Install [git](https://github.com/git-guides/install-git). On windows, use the Install Git on Windows section
- Type `git --version` to verify git was installed successfully.
- Connect your git locally : in the VSCode terminal run `git config user.email "MAIL_ADRESS"` and `git config user.name "USER_NAME"`

On Windows, to run command lines with a terminal, use "Window PowerShell" (or eventually "Invite de commande")


### 3. Create personal token and clone this repository locally

Create personal token
1) Follow [the instructions](https://gitlab-forge.din.developpement-durable.gouv.fr/help/user/profile/personal_access_tokens.md#create-a-personal-access-token) to create your personnal access token with read_repository, write_repository rights.
Note: Do not use "_" in the name of your token.
Note-2: you can choose how long your token will last, try to choose a date posterior to the end of your contribution to the project.

Clone the repository locally using either VS code or a terminal
- With the terminal
1) Open the terminal, and go into your folder: `cd <path/to/folder>`
2) Run `git clone https://TOKEN-NAME:TOKEN-PWD@GITLAB_URL`
3) Run `cd dbt` to enter the repository.
- With VS code
1) Go the folder
2) Search for the command clone and paste the url `https://TOKEN-NAME:TOKEN-PWD@GITLAB_URL`

# If your token is expired :
1) Create a new personal token (like above).
2) Clone the repository (like above).
3) Run `git config user.mail "MAIL_ADRESS"`.
4) Run `git config user.name "USER_NAME"`.
5) Copy your `secrets.env` file in bloc-notes for instance.
5) Reopen VS code in your new folder (in your explorer, your `secrets.env` file should no longer be).
6) Recreate a `secrets.env` file, by pasting your previous one.
Note: The modifications you did with your old token that where not merged to the main or published, won't be in your new folder, if you want to retrieve them, open in a new VS code window your old folder, and copy-past the modifications you want to retrieve. 

### 4. Install python and dbt
To install dbt, you can use homebrew on MacOs and pip otherwise. Instructions are [here](https://docs.getdbt.com/dbt-cli/installation/#pip)

Note - For this step (especially step 3) you have to be outside the ministery network. Use your phone connexion sharing or a personal WIFI and don't activate your VPN.

On Windows, we recommand to use pip. You need first to install python and then install dbt using pip command
1) [Install python](https://www.python.org/downloads/) locally on your computer (on your user only if you don't have admin rights on the whole computer).
2) Type `python --version` to verify python was installed. Then, type `pip --version` to verify python was installed. 
Note - You can also check py --version. It can take a few hours to have python --version and pip --version working.
3) Run `pip install -r requirements-tests.txt` to install dbt and all the required additional packages.
4) Check that it worked running `dbt --version`. 
5) Open with your code editor the gitlab dbt folder you previoulsly cloned. On windows, use VS code. VS code should automatically detect that it is a git folder

In VS code, you can open a Powershell terminal directly and use it to run command without having an additional power shell terminal window.

### 5. Postgres credentials & connection

1) Get the credentials for the local database or remote datebase.
2) Create a file `secrets.env` locally with the following environment variables:

```.env
POSTGRESQL_HOST=
POSTGRESQL_DB=
POSTGRESQL_USER=
POSTGRESQL_PASSWORD=
POSTGRESQL_SCHEMA=
POSTGRESQL_PORT=
B3_ACCESS_KEY_ID=
B3_SECRET_ACCESS_KEY=
B3_ENDPOINT_URL=https://b3.din.developpement-durable.gouv.fr
B3_REGION=RegionOne
B3_BUCKET_NAME=dbt_<PROJECT_NAME>
```

3) Export these variables running:
- on linux: `export $(grep -v '^#' secrets.env | xargs)`
- on Windows: `for /F %A in (secrets.env) do SET %A`
- on Powershell (you can use the powershell terminal directly in VS code): 
    ```
    get-content secrets.env | foreach {
        $name, $value = $_.split('=')
        set-content env:\$name $value
    }
    ```

4) Execute the debug command from your project to confirm that you can successfully connect: `dbt debug`.
Confirm that the last line of the output is Connection test: OK connection ok.

CONGRATS !! You achieved the installation process !
You are ready to start !

## Development

### 1. Use git process to developp

1 - Checkout to main branch and git pull to synchronise
2 - Create a new branch for your development
3 - Test your code locally with sqlfluff before commiting and pushing
4 - Commit and Push the branch to the gitlab repository
5 - Create a pull request to merge the branch to main when ready
    a) Commit and push the change on your branch
    b) Checkout to main branch and pull main to synchronise last changes
    c) Checkout to your branch and merge main to synchronise last changes
    d) Push the branch to the gitlab repository
    e) Create a pull request on gitlab to merge to main 
[Article Git workflow explained](https://medium.com/@swinkler/git-workflow-explained-a-step-by-step-guide-83c1c9247f03) 

### 2. Test your code locally

We use sql fluff to test verify the sql code respects some conventions. It runs in gitlab at each commit. However, you can check this locally on your computer.
1) In .sqlfluff, decomment the line with target = local and comment the one with target = sqlfluff. It enables sqlfluff to connect to the EcoSQL server to compile and test code.
2) Run 
    - `sqlfluff lint --processes 4 models/` to test all the sql code
    - `sqlfluff lint PATH_SQL_FILE` to test a specific sql file
    - `sqlfluff fix --processes 4 models/` to fix all the sql code
    - `sqlfluff fix PATH_SQL_FILE` to fix a specific sql file
3) When finish, in .sqlfluff, don't forget to comment the line with target = local and decomment the one with target = sqlfluff


### 3. Run commands locally

You can run dbt locally to test your code with those useful command lines

Note - You have to be outside the ministery network to run `dbt depts` or `pip install` which install packages. Use your phone connexion sharing or a personal WIFI and don't activate your VPN.

- Run a specific model: `dbt run -m <model_name>`. model_name can be a repository.
- Run all models: `dbt run`
- Test your connection: `dbt debug`
- Run tests: `dbt test`
- Generate Documentation: `dbt docs generate`
- Open documentation local website: `dbt docs serve`
- Copy csv files into DB: `dbt seed`
- Install dependencies: `dbt deps`
- Run, Test, Seed: `dbt build`

In order to upgrade dbt version, update `requirements.txt` or eventually `requirements-tests.txt` and:
- Run `pip install -r requirements.txt` or `pip install -r requirements-tests.txt`
- Run `dbt deps`
- Run `dbt debug` to check that it works as expected

You may have to uninstall packages first.

### 4. Documentation & tests

It is essential to document and test your code. For each sub-folder inside the model folder create a file `FOLDER_NAME_schema.yml`. You will create your documentation inside those files.

Documentation
Create your template
- `$ dbt run-operation generate_model_yaml --args '{"model_names": [ "MODEL_NAME" ] }'` creates the required structure to document your model in your YML file
- `$ dbt run-operation generate_model_yaml --args '{"model_names": [ "MODEL1_NAME", "MODEL2_NAME" ] }'` creates the required structure for several models
- `$ dbt ls -m models/FOLDER_PATH --output name | xargs -I{} echo -n ' "{}",'` list all the model from your folder to paste in the previous command

Using columns_definitions.md and dock block for field description
In order to have a unified centralised description of the columns and to prevent from repeating the same code accross multiple models (DRY - Don't Repeat Yourself), we use the columns_definitions.md file.

Every columns descriptions are defined in this file using doc blocks.
```
{% docs column_COLUMN_NAME %}  
    example of description for COLUMN_NAME
{% enddocs %}
```

Then, you can refer to this description in the schema.yml file
```
models:
  - name: example1_model
    description: "a first model as an example"
    columns:
      - name: column1
        description: "{{ doc('column_column1') }}"
  
  - name: example2_model
    description: "a second model to show how to reference model wit {{ ref() }}"
    columns:
      - name: column1
        description: "{{ doc('column_column1') }}"
    - name: column2
      description: "{{ doc('column_column2') }}"
```

For a column which is the primary_key of the model, you can add a pre-fixe PK - before the doc block reference.
`description: "PK - {{ doc('column_column1') }}"`

Pour générer la documentation, vous pouvez lancer :
- Generate Documentation: `dbt docs generate`
- Open documentation local website: `dbt docs serve` --> open the associated url

More information in this [article](https://docs.getdbt.com/blog/generating-dynamic-docs-dbt#get-model-names-programmatically)

Test
It is essential to add test on your model at least on the unicity of the primary(s) key(s) and on non null value for primary key.
You add tests in the schema.yml file.
```
  - name: example2_model
    description: "a second model to show how to reference model wit {{ ref() }}"
    columns:
      - name: column1
        description: "PK - {{ doc('column_column1') }}"
        tests :
          - unique
          - not_null
```

- Run tests: `dbt test`

More information in this [article](https://docs.getdbt.com/docs/build/tests)


## Deployment

### Prod

The gitlab CI/CD manages the deployment for dbt models.

Check [here](.gitlab-ci.yml) the CI/CD file.
Environment variables are defined in a file `.main.env`
You need to define these variables in the CI/CD section in gitlab!

For now, we only have one local and one PROD environment !
To be improved.

**Continous deployment**

When we push to the **main** branch, a pipeline is triggered to:
<!-- 1) NOT YET INSTALLED - Get the last state from the manifest file stored in b3 using AWS CLI -->
2) Run dbt dependencies (packages)
3) Build models (dbt seed, run and test) for only modified (and children) models
4) Export state (manifest.json) to b3 using AWS CLI

<!-- **Scheduling jobs**
NOT YET INSTALLED
We use the _schedule_ trigger offered by Gitlab to schedule our jobs. To do so, we add _rules_ in the CI/CD and set the 
_state_ parameter to an empty string to trigger all jobs (not only modified ones). -->

## Coding Conventions

Find more information [here](README%20Coding%20Conventions.md)

## Resources

- Learn more about dbt [in the docs](https://docs.getdbt.com/docs/introduction)
- Check out [Discourse](https://discourse.getdbt.com/) for commonly asked questions and answers
- Join the [chat](http://slack.getdbt.com/) on Slack for live discussions and support
- Find [dbt events](https://events.getdbt.com) near you
- Check out [the blog](https://blog.getdbt.com/) for the latest news on dbt's development and best practices
