with contours as (
    select * from {{source('brut','communes_ign')}}
)

, ref_com as (
    select
        codegeo as insee_com
        , pop as population
        , superficie as superf
    from {{ref('ref_communes')}}
)

, contours_modif as (
    select 
        nom
        ,nom_m
        ,insee_com
        ,population
        ,insee_dep
        ,insee_reg
        ,siren_epci
        , case 
            when ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.001) is not null
                then ST_MakeValid(ST_SimplifyPreserveTopology(ST_GeomFromText(geometry,4326),0.001))
            else ST_MakeValid(ST_SimplifyPreserveTopology(geometry,4326))
        end as geom
        from contours
)

, contours_finaux as (
    select
        cm.insee_com as codegeo
        , cm.nom
        , cm.nom_m
        , cm.insee_com as insee_com 
        , cm.population as pop
        , ref_com.superf
        , cm.insee_dep as insee_dep
        , cm.insee_reg as insee_reg
        , cm.siren_epci
        , cm.geom as geom
        , 'commune' as type_geo
        , ST_X(ST_CENTROID(cm.geom)) as longitude
        , ST_Y(ST_CENTROID(cm.geom)) as latitude
    from contours_modif cm
    join ref_com
    on ref_com.insee_com = cm.insee_com
)

select * from contours_finaux