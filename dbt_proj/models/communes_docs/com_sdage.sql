with brut_communes_sdages as (
    select 
        cdcommune as codegeo
        , concat(cdcommune, ';-;', 'SDAGE_', numcircadminbassin) as id
        , concat('SDAGE_',numcircadminbassin) as code_doc
        , 'SDAGE' as type_doc
    from {{source('brut','communes_sdages')}}
)

, i_sdage as (
    select code_doc
    from {{ ref('i_sdage' )}}
)

, passage as (
    select * from {{ref('ref_concatenation_passages')}}
)

{{ changement_codegeo('brut_communes_sdages', 'codegeo', 'id', 'passage', 2023) }}

select distinct on(code_doc, codegeo)
    i.code_doc as code_doc
    , m.new_codegeo as codegeo
    , 'SDAGE' as type_doc
from macro_brut_communes_sdages m
join i_sdage i
on substr(m.id, position(';-;' in m.id) + 3) = i.code_doc
