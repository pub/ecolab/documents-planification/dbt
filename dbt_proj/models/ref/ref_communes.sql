with superficie_communes as (
    select * from {{ source('brut', 'base_cc_comparateur') }}
)

, communes_ign as (
    select * from {{ source('brut', 'communes_ign')}}
)

, communes as (
    select
        communes_ign.insee_com as codegeo
        , left(communes_ign.siren_epci, 9) as epci
        , communes_ign.insee_dep as dep
        , communes_ign.insee_reg as reg
        , communes_ign.population::bigint as pop
        , superficie_communes.superf::float  as superficie
    from communes_ign
    left join superficie_communes on superficie_communes.codgeo = communes_ign.insee_com
)

select * from communes
