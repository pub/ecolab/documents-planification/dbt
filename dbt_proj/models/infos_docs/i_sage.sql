with liste_sages as (
    select * from {{ source('brut', 'liste_sages') }}
)

, dates_sages as (
    select * from {{ source('brut', 'dates_sages') }}
)

, lien_sages as (
    select * from {{ source('brut', 'lien_sages') }}
)

, regles_sages as (
    select * from {{ source('brut', 'regles_sages') }}
) 

, sage_sdage as (
    select * from {{ source('brut', 'sage_sdage') }}
) 

, infos_sages as (
    select
        ls.comite_de_bassin_responsable as structure_porteuse
        , ls.code_du_sage as code_doc
        , CONCAT('SAGE ',ls.nom_du_sage) as nom_doc
        , ls.etat_d_avancement as etat_document
        , ls.sous_etat_d_avancement as sous_etat
        , liens.site_internet as site_web
        , CONCAT('https://www.gesteau.fr/sage/',replace_special_characters(REPLACE(REPLACE(LOWER(ls.nom_du_sage),' - ','-'),' ','-'))) as lien_centralise
        , null as liens_docs
        , CASE
            WHEN ds.deliberation_finale_de_la_cle_2eme_revision IS NOT NULL THEN 'deliberation_finale_de_la_cle_2eme_revision' 
            WHEN ds.enquete_publique_2eme_revision IS NOT NULL THEN 'enquete_publique_2eme_revision' 
            WHEN ds.avis_du_comite_de_bassin_2eme_revision IS NOT NULL THEN 'avis_du_comite_de_bassin_2eme_revision' 
            WHEN ds.consultation_des_collectivites_2eme_revision IS NOT NULL THEN 'consultation_des_collectivites_2eme_revision' 
            WHEN ds.validation_du_projet_par_la_cle_2eme_revision IS NOT NULL THEN 'validation_du_projet_par_la_cle_2eme_revision' 
            WHEN ds.decision_de_mise_en_revision_du_sage_2eme_revision IS NOT NULL THEN 'decision_de_mise_en_revision_du_sage_2eme_revision' 
            WHEN ds.arrete_d_approbation_du_sage_apres_la_deuxieme_revision IS NOT NULL THEN 'arrete_d_approbation_du_sage_apres_la_deuxieme_revision' 
            WHEN ds.deliberation_finale_de_la_cle_2 IS NOT NULL THEN 'deliberation_finale_de_la_cle_2'
            WHEN ds.enquete_publique_2 IS NOT NULL THEN 'enquete_publique_2'
            WHEN ds.avis_du_comite_de_bassin_2 IS NOT NULL THEN 'avis_du_comite_de_bassin_2'
            WHEN ds.consultation_des_collectivites2 IS NOT NULL THEN 'consultation_des_collectivites2'
            WHEN ds.validation_du_projet_par_la_cle IS NOT NULL THEN 'validation_du_projet_par_la_cle'
            WHEN ds.decision_de_mise_en_revision_du_sage IS NOT NULL THEN 'decision_de_mise_en_revision_du_sage'
            WHEN ds.arrete_d_approbation_du_sage_apres_la_premiere_revision IS NOT NULL THEN 'arrete_d_approbation_du_sage_apres_la_premiere_revision'
            WHEN ds.arrete_modificatif_d_approbation_du_sage IS NOT NULL THEN 'arrete_modificatif_d_approbation_du_sage'
            WHEN ds.arrete_d_approbation_du_sage IS NOT NULL THEN 'arrete_d_approbation_du_sage'
            WHEN ds.deliberation_finale_de_la_cle_1 IS NOT NULL THEN 'deliberation_finale_de_la_cle_1'
            WHEN ds.enquete_publique_1 IS NOT NULL THEN 'enquete_publique_1'
            WHEN ds.avis_du_comite_de_bassin_1 IS NOT NULL THEN 'avis_du_comite_de_bassin_1'
            WHEN ds.consultation_des_collectivites1 IS NOT NULL THEN 'consultation_des_collectivites1'
            WHEN ds.validation_du_projet_de_sage_par_la_cle IS NOT NULL THEN 'validation_du_projet_de_sage_par_la_cle'
            WHEN ds.validation_du_choix_de_la_strategie IS NOT NULL THEN 'validation_du_choix_de_la_strategie'
            WHEN ds.validation_des_tendances_et_des_scenarios IS NOT NULL THEN 'validation_des_tendances_et_des_scenarios'
            WHEN ds.validation_du_diagnostic IS NOT NULL THEN 'validation_du_diagnostic'
            WHEN ds.validation_de_l_etat_des_lieux IS NOT NULL THEN 'validation_de_l_etat_des_lieux'
            WHEN ds.reunion_institutive IS NOT NULL THEN 'reunion_institutive'
            WHEN ds.derniere_modification_de_l_arrete_de_la_cle IS NOT NULL THEN 'derniere_modification_de_l_arrete_de_la_cle'
            WHEN ds.arrete_de_renouvellement_de_la_cle IS NOT NULL THEN 'arrete_de_renouvellement_de_la_cle'
            WHEN ds.arrete_de_creation_de_la_cle IS NOT NULL THEN 'arrete_de_creation_de_la_cle'
            WHEN ds.derniere_modification_de_l_arrete_de_perimetre IS NOT NULL THEN 'derniere_modification_de_l_arrete_de_perimetre'
            WHEN ds.arrete_de_perimetre IS NOT NULL THEN 'arrete_de_perimetre'
            WHEN ds.consultation_du_comite_de_bassin IS NOT NULL THEN 'consultation_du_comite_de_bassin'
            WHEN ds.consultation_des_communes IS NOT NULL THEN 'consultation_des_communes'
            WHEN ds.dossier_preliminaire IS NOT NULL THEN 'dossier_preliminaire'
            WHEN ds.reflexion_prealable IS NOT NULL THEN 'reflexion_prealable'
            ELSE NULL
        END as type_evt
        , COALESCE(ds.deliberation_finale_de_la_cle_2eme_revision,ds.enquete_publique_2eme_revision, ds.avis_du_comite_de_bassin_2eme_revision, ds.consultation_des_collectivites_2eme_revision,ds.validation_du_projet_par_la_cle_2eme_revision,ds.decision_de_mise_en_revision_du_sage_2eme_revision,ds.arrete_d_approbation_du_sage_apres_la_deuxieme_revision,ds.deliberation_finale_de_la_cle_2,ds.enquete_publique_2,ds.avis_du_comite_de_bassin_2,ds.consultation_des_collectivites2,ds.validation_du_projet_par_la_cle,ds.decision_de_mise_en_revision_du_sage,ds.arrete_d_approbation_du_sage_apres_la_premiere_revision,arrete_modificatif_d_approbation_du_sage,ds.arrete_d_approbation_du_sage,ds.deliberation_finale_de_la_cle_1,ds.enquete_publique_1,ds.avis_du_comite_de_bassin_1,ds.consultation_des_collectivites1,ds.validation_du_projet_de_sage_par_la_cle,ds.validation_du_choix_de_la_strategie,ds.validation_des_tendances_et_des_scenarios,ds.validation_du_diagnostic,ds.validation_de_l_etat_des_lieux,ds.reunion_institutive,ds.derniere_modification_de_l_arrete_de_la_cle,ds.arrete_de_renouvellement_de_la_cle,ds.arrete_de_creation_de_la_cle,ds.derniere_modification_de_l_arrete_de_perimetre,ds.arrete_de_perimetre,ds.consultation_du_comite_de_bassin,ds.consultation_des_communes,ds.dossier_preliminaire,ds.reflexion_prealable)
        as date_evt
        , ds.arrete_d_approbation_du_sage as date_creation
        , null as date_expiration
        , 'SAGE' as type_doc
        , null as contact
        , ss.identifie_necessaire_dans_le_sdage_2022_2027 as option_necessaire_sdage
    from liste_sages ls
    left join dates_sages ds on ls.code_du_sage = ds.code_du_sage
    left join lien_sages liens on ls.code_du_sage = liens.code_du_sage
    left join sage_sdage ss on ls.code_du_sage = ss.code_du_sage
) 

select * from infos_sages
