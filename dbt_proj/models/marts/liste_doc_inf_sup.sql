with docs_communes as (
    select 
        t1.*
        , r.epci
        , r.dep
        , r.reg
    from {{ref('com_docs')}} t1
    join {{ref('ref_communes')}} r
    on t1.codegeo = r.codegeo
)

, liste_communes as (
    select * from {{ref('ref_communes')}}
)

, communes as (
    select distinct on(t1.code_doc,codegeo) 
        t1.code_doc
        , codegeo as codegeo
        , 'commune' as type_geo
        , case 
            when t2.nb>1 then 'contient'
            when t2.nb=1 then 'égal'
        end as niveau
        , type_doc
    from docs_communes t1
    join (
        select code_doc
            , COUNT(distinct codegeo) as nb
        from {{ref('com_docs')}}
        group by code_doc) t2 
    on t1.code_doc=t2.code_doc
)

, epcis as (
    select distinct on (t1.code_doc,epci) 
        t1.code_doc
        , t1.epci as codegeo
        , 'epci' as type_doc
        , case
            when t3.nb_epci>1 and t2.nb_com_doc_epci = b.nb_com_epci then 'contient'
            when t3.nb_epci>1 and t2.nb_com_doc_epci != b.nb_com_epci then 'transverse'
            when t3.nb_epci=1 and t2.nb_com_doc_epci = b.nb_com_epci then 'égal'
            when t3.nb_epci=1 and t2.nb_com_doc_epci != b.nb_com_epci then 'inclus'
        end as niveau
        , type_doc
    from (
        select distinct on(epci,code_doc) *
        from docs_communes) t1
    join (
        select code_doc
        , count(distinct(epci)) as nb_epci
        from docs_communes
        group by code_doc) t3 
    on t3.code_doc=t1.code_doc
    join (
        select code_doc
        , count(codegeo) as nb_com_doc_epci
        from docs_communes
        group by code_doc,epci) t2 
    on t1.code_doc=t2.code_doc
    join (
        select count(codegeo) as nb_com_epci
        , epci as siren  
        from liste_communes
        group by siren) b 
    on b.siren=t1.epci
)

, dep as(
    select distinct on (t1.code_doc, t1.dep) 
    t1.code_doc
    , t1.dep as codegeo
    , 'dep' as type_geo
    , case
        when t3.nb_dep>1 and t2.nb_com_doc_dep = b.nb_com_dep then 'contient'
        when t3.nb_dep>1 and t2.nb_com_doc_dep != b.nb_com_dep then 'transverse'
        when t3.nb_dep=1 and t2.nb_com_doc_dep = b.nb_com_dep then 'égal'
        when t3.nb_dep=1 and t2.nb_com_doc_dep != b.nb_com_dep then 'inclus'
    end as niveau
    , type_doc
    from (
        select distinct on(dep,code_doc) *
        from docs_communes) t1
    join (
        select code_doc
        , count(distinct(dep)) as nb_dep
        from docs_communes
        group by code_doc) t3 
    on t3.code_doc=t1.code_doc
    join (
        select code_doc
        , count(codegeo) as nb_com_doc_dep
        from docs_communes
        group by code_doc,dep) t2 on t1.code_doc=t2.code_doc
    join (
        select count(codegeo) as nb_com_dep
        , dep  
        from liste_communes 
        group by dep) b 
    on b.dep=t1.dep
)

, reg as (
    select distinct on(t1.code_doc,t1.reg) 
    t1.code_doc
    , t1.reg as codegeo
    , 'reg' as type_geo
    , case
        when t3.nb_reg>1 and t2.nb_com_doc_reg = b.nb_com_reg then 'contient'
        when t3.nb_reg>1 and t2.nb_com_doc_reg != b.nb_com_reg then 'transverse'
        when t3.nb_reg=1 and t2.nb_com_doc_reg = b.nb_com_reg then 'égal'
        when t3.nb_reg=1 and t2.nb_com_doc_reg != b.nb_com_reg then 'inclus'
    end as niveau
    , type_doc
    from (
        select distinct on(reg,code_doc) *
        from docs_communes) t1
    join (
        select 
        code_doc
        , count(distinct(reg)) as nb_reg
        from docs_communes
        group by code_doc) t3 
    on t3.code_doc=t1.code_doc
    join (
        select code_doc
        , count(codegeo) as nb_com_doc_reg
        from docs_communes
        group by code_doc,reg) t2 
    on t1.code_doc=t2.code_doc
    join (
        select count(codegeo) as nb_com_reg
        , reg  
        from liste_communes
        group by reg) b 
    on b.reg=t1.reg
)

, union_niveaux as (
    select * from reg
    union
    select * from dep
    union
    select * from epcis
    union
    select * from communes
)

select * from union_niveaux